'''
                              ESP Health Project
                                qmetric Module
                               URL Configuration

@authors: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics Inc. http://www.commoninf.com
@copyright: (c) 2014 Commonwealth Informatics Inc.
@license: LGPL
'''


from ESP.qmetric.views import create_urls

urlpatterns = create_urls()